from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm
# Create your views here.


@login_required
def receipt_list_view(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": receipts
    }
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt_view(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def expense_category_list_view(request):
    expenses = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "expenses_list": expenses
    }
    return render(request, "receipts/expense_list.html", context)


@login_required
def account_list_view(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts_list": accounts
    }
    return render(request, "receipts/account_list.html", context)


@login_required
def create_expense_category_view(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            expense = form.save(False)
            expense.owner = request.user
            expense.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {
        "form": form
    }
    return render(request, "receipts/expcat_detail.html", context)


def create_account_view(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form": form
    }
    return render(request, "receipts/account_detail.html", context)
